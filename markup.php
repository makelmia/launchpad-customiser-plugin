<div id="lpc-options">
  <div class="lpc-options-wrapper">
  <div class="lpc-options-sidebar">
    <h3>Customise Popup</h3>
    <ul>
        <li><a href="#tab_color"><img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/SVG/colour.svg' ?>" > Color</a></li>
        <li><a href="#tab_design"><img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/SVG/design.svg' ?>" > Design</a></li>
        <li><a href="#tab_header"><img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/SVG/header.svg' ?>" > Header</a></li>
        <li><a href="#tab_elements"><img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/SVG/elements.svg' ?>" > Elements</a></li>
        <li><a href="#tab_footer"><img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/SVG/footer.svg' ?>" > Footer</a></li>
      </ul>
  </div>
  <div class="lpc-options-content">
    <div id="tab_color">
      <div class="color_scheme">
        <h3 class="option_title">Select colour scheme</h3>
        <label for="lp-style-light">
          <input type="radio" name="lp_style" id="lp-style-light" value="lp-style-light">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/light.png'; ?>"> Light
        </label>
        <label for="lp-style-dark">
            <input type="radio" name="lp_style" id="lp-style-dark" value="lp-style-dark">
            <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/icons/dark.png'; ?>"> Dark
        </label>
      </div>
      <div class="site_color">
        <h3 class="option_title">Select your colour</h3>
        <label for="lp-theme-E3DBD8" style="background-color: #E3DBD8;">
          <input type="radio" name="lp_color" value="#E3DBD8">
        </label>
        <label for="lp-theme-A7ABB7" style="background-color: #A7ABB7;">
          <input type="radio" name="lp_color" value="#A7ABB7">
        </label>
        <label for="lp-theme-44575E" style="background-color: #44575E;">
          <input type="radio" name="lp_color" value="#44575E">
        </label>
        <label for="lp-theme-353535" style="background-color: #353535;">
          <input type="radio" name="lp_color" value="#353535">
        </label>
        <label for="lp-theme-FBDCCA" style="background-color: #FBDCCA;">
          <input type="radio" name="lp_color" value="#FBDCCA">
        </label>
        <label for="lp-theme-FFDE5D" style="background-color: #FFDE5D;">
          <input type="radio" name="lp_color" value="#FFDE5D">
        </label>
        <label for="lp-theme-CAE9BF" style="background-color: #CAE9BF;">
          <input type="radio" name="lp_color" value="#CAE9BF">
        </label>
        <label for="lp-theme-B6E8F3" style="background-color: #B6E8F3;">
          <input type="radio" name="lp_color" value="#B6E8F3">
        </label>
        <label for="lp-theme-DEE1E6" style="background-color: #DEE1E6;">
          <input type="radio" name="lp_color" value="#DEE1E6">
        </label>
        <label for="lp-theme-A11450" style="background-color: #A11450;">
          <input type="radio" name="lp_color" value="#A11450">
        </label>
        <label for="lp-theme-A71D1D" style="background-color: #A71D1D;">
          <input type="radio" name="lp_color" value="#A71D1D">
        </label>
        <label for="lp-theme-275D2B" style="background-color: #275D2B;">
          <input type="radio" name="lp_color" value="#275D2B">
        </label>
        <label for="lp-theme-11549A" style="background-color: #11549A;">
          <input type="radio" name="lp_color" value="#11549A">
        </label>
        <label for="lp-theme-6C42A0" style="background-color: #6C42A0;">
          <input type="radio" name="lp_color" value="#6C42A0">
        </label>
      </div>
    </div>
    <div id="tab_design">
      <h3 class="option_title">Select your design</h3>
      <div class="tab-options">
        <label for="lp-design-1">
          <input type="radio" name="lp_design" value="lp-design-1">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Design-1.jpg'; ?>"> Design 1
        </label>
        <label for="lp-design-2">
          <input type="radio" name="lp_design" value="lp-design-2">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Design-2.jpg'; ?>"> Design 2
        </label>
        <label for="lp-design-3">
          <input type="radio" name="lp_design" value="lp-design-3">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Design-3.jpg'; ?>"> Design 3
        </label>
      </div>
    </div>
    <div id="tab_header">
      <h3 class="option_title">Select your header</h3>
      <label for="lp-header-1">
        <input type="radio" name="lp_header" id="lp-header-1" value="lp-header-1">
        <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/header-1.jpg'; ?>">
      </label>
      <label for="lp-header-2">
        <input type="radio" name="lp_header" id="lp-header-2" value="lp-header-2">
        <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/header-1.jpg'; ?>">
      </label>
      <label for="lp-header-3">
        <input type="radio" name="lp_header" id="lp-header-3" value="lp-header-3">
        <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/header-3.jpg'; ?>">
      </label>
    </div>
    <div id="tab_footer">
        <h3 class="option_title">Select your footer</h3>
        <label for="lp-footer-1">
          <input type="radio" name="lp_footer" id="lp-footer-1" value="lp-footer-1">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/footer-1.png'; ?>">
        </label>
        <label for="lp-footer-2">
          <input type="radio" name="lp_footer" id="lp-footer-2" value="lp-footer-2">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/footer-2.png'; ?>">
        </label>
    </div>
    <div id="tab_elements">
      <div class="tab-options">
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-form-element" value="lp-show-form-element">
        <label for="lp-show-form-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Form.png'; ?>"> Form
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-properties-element" value="lp-show-properties-element">
        <label for="lp-show-properties-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Properties.png'; ?>"> Properties
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-team-element" value="lp-show-team-element">
        <label for="lp-show-team-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Team.png'; ?>"> Team
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-reviews-element" value="lp-show-reviews-element">
        <label for="lp-show-reviews-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Reviews.png'; ?>"> Reviews
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-about-us-element" value="lp-show-about-us-element">
        <label for="lp-show-about-us-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/About-Us.png'; ?>"> About Us
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-our-services-element" value="lp-show-our-services-element">
        <label for="lp-show-our-services-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Our-Services.png'; ?>"> Our Services
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-property-appraisal-element" value="lp-show-property-appraisal-element">
        <label for="lp-show-property-appraisal-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Property-Appraisal.png'; ?>"> Property Appraisal
        </label>
        <input type="checkbox" name="lp-show-hide-element" id="lp-show-posts-element" value="lp-show-posts-element">
        <label for="lp-show-posts-element">
          <img src="<?php echo plugin_dir_url( __FILE__ ) . '/assets/img/Posts.png'; ?>"> Posts
        </label>
      </div>
    </div>
  <div class="lpc_options_footer"><button id="lpc-cancel">Clear</button><button id="lpc-update">Save</button></div>
  </div>
  </div>

</div>
  <div id="lpc-helper"><i class="fa fa-pencil" aria-hidden="true"></i></div>
<script>
    $(function() {
      $( "#lpc-options" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
      $( "#lpc-options li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
      $( "#lpc-options" ).tabs({
        event: "click"
      });
     });
     $('#lpc-options').tabs({ active: true, collapsible: true });
</script>
<script>
$(document).ready(function(){
  	$("#lpc-update").click(function(event){
  		$("#lpc-options").toggleClass('is-visible');
      location.reload();


      event.preventDefault();
      var $element_class = [];
      $("#tab_elements input:checkbox:checked").map(function(){
          $element_class.push($(this).val());
      });
      var $elemt_class_trim = $element_class.join(' ');
      $.session.set('var_element_class',$elemt_class_trim);
  	});
    $("#lpc-cancel").click(function(){
      $('body').removeClass("lp-theme-red");
      $('body').removeClass("lp-theme-blue");
      $('body').removeClass("lp-theme-green");
      $('body').removeClass("lp-theme-black");

      $('body').removeClass("lp-design-1");
      $('body').removeClass("lp-design-2");
      $('body').removeClass("lp-design-3");

      $('body').removeClass("lp-header-transparent");
      $('body').removeClass("lp-header-light");
      $('body').removeClass("lp-header-dark");
      $('body').removeClass("lp-header-theme");

      $.session.clear();
  	});
  	$('#lpc-helper').on('click', function(e) {
  	  e.preventDefault();
  	  $('#lpc-options').toggleClass('is-visible');
  	});
  	$('#tab_color .site_color label').click(function() {
  		$('#tab_color .site_color label.active').removeClass('active');
  		$(this).addClass('active'); // add onto current
      var $color_class = $("input",this).attr("value");
      $.session.set('var_color_class',$color_class);

  	});
    $('#tab_color .color_scheme label').click(function() {
  		$('#tab_color .color_scheme label.active').removeClass('active');
  		$(this).addClass('active'); // add onto current
      var $color_scheme_class = $("input",this).attr("value");
      $.session.set('var_color_scheme_class',$color_scheme_class);

  	});
  	$('#tab_design label').click(function() {
  		$('#tab_design label.active').removeClass('active');
  		$(this).addClass('active'); // add onto current
      var $design_class = $("input",this).attr("value");
      $.session.set('var_design_class',$design_class);
      if( $design_class == 'lp-design-1'){
        $('body').removeClass("lp-design-2");
        $('body').removeClass("lp-design-3");
        $('body').toggleClass($design_class);
        $.session.set("var_design_class", $design_class);
      } else if( $design_class == 'lp-design-2'){
        $('body').removeClass("lp-design-1");
        $('body').removeClass("lp-design-3");
        $('body').toggleClass($design_class);
        $.session.set("var_design_class", $design_class);
      } else if( $design_class == 'lp-design-3'){
        $('body').removeClass("lp-design-1");
        $('body').removeClass("lp-design-2");
        $('body').toggleClass($design_class);
        $.session.set("var_design_class", $design_class);
      }
  	});
  	$('#tab_header label').click(function() {
  		$('#tab_header label.active').removeClass('active');
  		$(this).addClass('active'); // add onto current
      var $header_class = $("input",this).attr("value");
      $.session.set('var_header_class',$header_class);
      if( $header_class == 'lp-header-transparent'){
        $('body').removeClass("lp-header-light");
        $('body').removeClass("lp-header-dark");
        $('body').removeClass("lp-header-theme");
        $('body').toggleClass($header_class);
        $.session.set("var_header_class", $header_class);
      } else if( $header_class == 'lp-header-light'){
        $('body').removeClass("lp-header-transparent");
        $('body').removeClass("lp-header-dark");
        $('body').removeClass("lp-header-theme");
        $('body').toggleClass($header_class);
        $.session.set("var_header_class", $header_class);
      } else if( $header_class == 'lp-header-dark'){
        $('body').removeClass("lp-header-transparent");
        $('body').removeClass("lp-header-light");
        $('body').removeClass("lp-header-theme");
        $('body').toggleClass($header_class);
        $.session.set("var_header_class", $header_class);
      } else if( $header_class == 'lp-header-theme'){
        $('body').removeClass("lp-header-transparent");
        $('body').removeClass("lp-header-light");
        $('body').removeClass("lp-header-dark");
        $('body').toggleClass($header_class);
        $.session.set("var_header_class", $header_class);
      }
  	});
  	$('#tab_footer label').click(function() {
  		$('#tab_footer label.active').removeClass('active');
  		$(this).addClass('active'); // add onto current
      var $footer_class = $("input",this).attr("value");
      $.session.set('var_footer_class',$footer_class);
  	});
  	$('#tab_style label').click(function() {
  		$('#tab_style label.active').removeClass('active');
  		$(this).addClass('active'); // add onto current
      var $style_class = $("input",this).attr("value");
      $.session.set('var_style_class',$style_class);
      if( $style_class == 'lp-style-light'){
        $('body').removeClass("lp-style-dark");
        $('body').toggleClass($style_class);
      } else if( $style_class == 'lp-style-dark'){
        $('body').removeClass("lp-style-light");
        $('body').toggleClass($style_class);
      }
  	});


    $.session.get("var_color_class");
    $.session.get("var_color_scheme_class");

});
</script>
<script>

$(document).ready(function(e){
var _get_color = $.session.get("var_color_class");
var _get_color_scheme = $.session.get("var_color_scheme_class");
$('body').addClass( _get_color_scheme );

$("ul.btn-hero li div.property-status, .current.page-numbers, .sidebar .agent-profile ul.agent-social-media li a:hover, .atrealty-design-1 .agent-profile.page-element:after, .section-title span:before, .content-box-element.image-left-content .hic-box .hic-title span:before, .styled-title .hic-title span:before, .styled-title span:before, .hic-box .hic-title span:before, .page-numbers:hover, .services-section .hic-icon:before, .agent-property-tabs .tabs-title>a[aria-selected=true], .agent-property-tabs .tabs-title>a:hover, .default-section.call-to-action .hic-content, .agent-profile .social-media a, .property-search-form-inner .button, .gform_wrapper.gravity-theme .gform_footer .button").css({"background-color": _get_color });

$(".atrealty-design-2 .hic-title span, .atrealty-design-2 .section-title span, .footer .contact-details i, .atrealty-design-2 .property-locations .hic-title h3 span, .page-element.has-bg-img .contact-details i, .page-element.has-bg-img .social-media i, .property-locations .property-location-area li:before, .atrealty-design-2 .property-element.image-above-content .price, .header .social-media-container a:hover, .single-property_type.header-transparent .header .social-media-container a:hover, .single-property-details .property-display-price, .read-more-trigger, .agent-contact-details > li:before, ul.agent-experiences li:before, ul.agent-awards li:before, .atrealty-design-1 .contact-us-form .gform_footer [type=submit], .atrealty-design-1 .contact-us-form .gform_footer:before, .atrealty-design-1 .agent-profile .contact-details > div:before, .atrealty-design-1 .agent-contact-details > a:before, .atrealty-design-1 .agent-profile .contact-details > div:hover a, .cst-testimonials .testimonial-item .hic-content .hic-blurb p::before, .services-section .hic-title:before, .agent-property-tabs .tabs-title>a, .text-link .hic-button-wrap a, .atrealty-design-1 .page-element.agent-profile .contact-details a, .atrealty-design-1 .agent-contact-details a, .propertyMapContainer::before, .inspect-date, .page-element:not(.has-bg-img):not(.theme-section) .section-title span, .section-title span, .content-box-element.image-left-content .hic-box .hic-title span, .hic-box .hic-title span, .styled-title .hic-title span, .styled-title span, .page-element.agent-profile .contact-details a, .agent-profile .contact-details > div, .agent-contact-details > a, body:not(.header-theme) .header .menu li.active > a, .contact-us-form .gform_footer [type=submit]").css({"color": _get_color});

var _get_design = $.session.get("var_design_class");
$('body').addClass( _get_design );

var _get_header = $.session.get("var_header_class");
$('body').addClass( _get_header );

var _get_footer = $.session.get("var_footer_class");
$('body').addClass( _get_footer );

var _get_element = $.session.get("var_element_class");
$('body').addClass( _get_element );

    if ($("body").hasClass("lp-show-form-element")) {
        $("#lp-show-form-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-properties-element")){
        $("#lp-show-properties-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-team-element")){
        $("#lp-show-team-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-reviews-element")){
        $("#lp-show-reviews-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-about-us-element")){
        $("#lp-show-about-us-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-our-services-element")){
        $("#lp-show-our-services-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-property-appraisal-element")){
        $("#lp-show-property-appraisal-element").attr("checked", "checked");
    }
    if ($("body").hasClass("lp-show-posts-element")){
        $("#lp-show-posts-element").attr("checked", "checked");
    }
var _get_style = $.session.get("var_style_class");
$('body').addClass( _get_style );
$.ajax({
       url: "/",
       method: "POST",
       data: "_get_color=" + _get_color,
      success: function(response) {}
    });


    /*on load*/
    var new_get_color = _get_color.substring(1, _get_color.length);
    var new_get_color_lable = '[for="lp-theme-' + new_get_color + '"]';
    if(_get_color != ''){
      $(new_get_color_lable).addClass('active');
    }

    var id_get_color_scheme = '#' + _get_color_scheme;
    if(_get_color_scheme != ''){
      $(id_get_color_scheme).attr('checked', 'checked');
    }

    var new_get_design_lable = '[for="' + _get_design + '"]';
    if(_get_design != ''){
      $(new_get_design_lable).addClass('active');
    }

    var new_get_header_lable = '[for="' + _get_header + '"]';
    if(_get_header != ''){
      $(new_get_header_lable).addClass('active');
    }

    var new_get_footer_lable = '[for="' + _get_footer + '"]';
    if(_get_footer != ''){
      $(new_get_footer_lable).addClass('active');
    }



});

</script>

<style>
.section-title span:before, .content-box-element.image-left-content .hic-box .hic-title span:before, .styled-title .hic-title span:before, .styled-title span:before, .hic-box .hic-title span:before{
  background-color: <?php echo $_get_color; ?>;
}
</style>
