<?php
/*
add_filter( 'body_class','lpc_classes' );
function lpc_classes( $classes ) {
    $classes[] = get_field('lpc_color', 'option');
		$classes[] = get_field('lpc_design', 'option');
		$classes[] = get_field('lpc_header', 'option');
		$classes[] = get_field('lpc_footer', 'option');
		$classes[] = get_field('lpc_style', 'option');
    return $classes;
}

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}
//CREATE ACF FORM AND OPTION PAGE
add_action( 'init', 'brandpage_form_head' );
function brandpage_form_head(){
  acf_form_head();
}

add_action('wp_head', 'acf_form_connection');
function acf_form_connection(){
  $options = array(
    'post_id' => 'options',
    'field_groups' => array('group_623a95a4ee58b'),
  );
  acf_form($options);
};*/
function LP_customiser_init(){
    if ( is_user_logged_in() )
    {
      //ADD LPC MARKUP
      add_action('wp_footer', 'lpc_markup');
      function lpc_markup(){
          include_once  __DIR__ . '/markup.php';
      };

    }
}
add_action('init', 'LP_customiser_init');

add_action('wp_enqueue_scripts','resources_init');
function resources_init() {
    wp_enqueue_script( 'built-js', plugins_url( '/resources/jquery-1.9.1.js', __FILE__ ));
    wp_enqueue_script( 'ui-js', plugins_url( '/resources/jquery-ui.js', __FILE__ ));
    wp_enqueue_script( 'lpc-js', plugins_url( '/assets/scripts.js', __FILE__ ));
    wp_enqueue_script( 'session-js', plugins_url( '/assets/jquery.session.min.js', __FILE__ ));
};
