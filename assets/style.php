<?php
function lpc_insert_head() {
	?>
	<style>
        /*Hi Star Rating - START*/
				#lpc-options{
					font-family: 'Red Hat Display', sans-serif;
				}
				.lpc-options-wrapper {
				    display: flex;
				    flex-wrap: wrap;
						text-align: center;
					 background: #FFFFFF;
					 box-shadow: 0 4px 8px rgb(0 0 0 / 20%);

					 max-width: 800px;
					 max-height: fit-content;
					 margin: auto;
	 					position: fixed;
	 					top: 0;
	 					left: 0;
	 					bottom: 0;
	 					right: 0;
	 					height: auto;
				}
				#lpc-options .lpc-options-sidebar {
				    padding: 20px 0;
				    width: 30%;
				    list-style: none;
				    text-align: left;
				    margin: 0;
				    flex: 1;
				    box-shadow: 8px 0 8px rgb(0 0 0 / 10%);
				}
				#lpc-options .lpc-options-content {
				    display: flex;
				    flex-wrap: wrap;
				    flex-direction: column;
						width: 70%;
    				padding: 3% 5%;
				}
				#lpc-options ul { margin: 0 !important; }
				#lpc-options ul li { clear: left; width: 100%; padding: 0; list-style: none;}
				#lpc-options ul li a { display:flex; }
				#lpc-options ul li.ui-tabs-active { }
				#lpc-options ul li img {
				    width: 25px;
				    height: auto;
				    margin-right: 10px;
				}
				#lpc-options .ui-tabs-panel {
			    float: left;
			    padding: 15px;
			    border-radius: 10px;
				}
				#lpc-options .lpc-options-sidebar h3 {
					font-size: 21px;
					color: #333 !important;
					letter-spacing: 1px;
					font-weight: bold;
					padding-left: 20px;
				}


				#lpc-helper {
					position: fixed;
					width: 60px;
					height: 60px;
					background: rgb(255 255 255 / 90%);;
					right: 10px;
					bottom: 10px;
					border-radius: 100%;
					color: #000;
					text-align: center;
					font-size: 25px;
					line-height: 60px;
					cursor: pointer;
					box-shadow: 0 3px 6px rgb(0 0 0 / 16%), 0 1px 2px rgb(0 0 0 / 23%);
					z-index: 10000;
				}
				#lpc-options {
					z-index: 999;
			    display: none;
			    opacity: 0;
			    transition: all .2s;
			    background: rgba(0,0,0,.5);
			    position: fixed;
			    width: 100%;
			    height: 100%;
			    top: 0;
				}
				#lpc-options.is-visible {
					display: block;
					opacity: 1;
					transition: all .2s;
				}
				#lpc-options ul li a {
					text-decoration: none;
					letter-spacing: 1px;
					padding: 20px 40px;
					font-size: 16px;
					color: #808080;
				}
				#lpc-options ul li.ui-tabs-active a {
					background: #EFEFEF;
					color: #000;
				}
				#lpc-options .ui-tabs-panel input[type=radio] {
					display: none;
				}
				#lpc-options .ui-tabs-panel label img {
					width: 50px;
					height: auto;
					border: 2px solid transparent;
				}
				#lpc-options .ui-tabs-panel label {
    				display: inline-block;
				    width: 100%;
				    padding: 0;
				    margin: 0;
				    cursor: pointer;
				    text-transform: uppercase;
				    line-height: normal;
				    margin-bottom: 10px;
				    font-size: 16px;
				    text-align: left;
				    letter-spacing: 1px;
				    color: #333 !important;
				}

				form#acf-form {
					display: none;
				}
				#message {
					display: none;
				}
				.lpc_options_footer {
				    width: 100%;
				    display: flex;
				    justify-content: center;
				}
				.lpc_options_footer button {
				    display: block;
				    background: #000;
				    clear: both;
				    margin-top: 20px;
				    float: right;
				    cursor: pointer;
				    padding: 10px 15px;
				    color: #fff;
				    border-radius: 5px;
				    text-transform: uppercase;
				    font-size: 12px;
				    min-width: 100px;
				    letter-spacing: 1px;
				    margin-right: 10px;
				}
				.lpc_options_footer button:hover {
				    background-color: transparent !important;
				    border-color: #000 !important;
				    color: #000 !important;
				}

				.lp-header-transparent #header,
				.lp-header-transparent #masthead{
					background-color: transparent !important;
				}
				.lp-header-light #header,
				.lp-header-light #masthead{
					background-color: #fff !important;
				}
				.lp-header-dark #header,
				.lp-header-dark #masthead{
					background-color: #000 !important;
				}
				.lp-header-theme #header,
				.lp-header-theme #masthead{
					background-color: #00adef !important;
				}

				.lp-theme-red{
					color: red !important;
				}
				.lp-theme-blue{
					color: blue !important;
				}
				.lp-theme-green{
					color: green !important;
				}
				.lp-theme-black{
					color: black !important;
				}

				.lp-footer-grey #footer{
					background-color: #eee !important;
				}
				.lp-footer-light #footer{
					background-color: #fff !important;
				}
				.lp-footer-dark #footer{
					background-color: #000 !important;
				}
				.lp-footer-theme #footer{
					background-color: #00adef !important;
				}

				.lp-style-light{
					background-color: #fff !important;
					color: #000;
				}
				.lp-style-dark{
					background-color: #000 !important;
					color: #fff;
				}


				#lpc-options .color_scheme {
					display: flex;
					justify-content: flex-start;
					margin-bottom: 40px;
					flex-wrap: wrap;
				}
				h3.option_title {
				    flex: 1 1 100%;
				    font-size: 18px;
				    text-align: left;
				    font-weight: 600;
				    color: #333333;
				    margin-bottom: 40px;
				}
				#lpc-options .ui-tabs-panel .color_scheme input[type=radio] {
				    display: block !important;
						margin-bottom: 0;
				    height: 20px;
				    width: 20px;
				}
				#lpc-options .ui-tabs-panel .color_scheme label {
				    display: flex !important;
				    align-items: center;
				    width: max-content;
				    margin-bottom: 0;
				    margin-right: 10px;
				}
				#lpc-options .ui-tabs-panel .color_scheme label img {
				    width: 17px;
				    height: auto;
				    border: 0;
				    margin-right: 5px;
				}
				#lpc-options .site_color label {
				    width: 60px;
				    height: 60px;
				    border-radius: 100%;
				    margin: 10px;
				}
				#lpc-options .site_color label.active {
				    border: 3px solid white;
				    box-shadow: 0 0 8px rgb(0 0 0 / 20%);
				}
				#lpc-options .tab-options label {
				    display: inline-grid;
				    margin: 0;
				    text-align: center;
				}
				#lpc-options .tab-options label.active img {
				    box-shadow: 0 2px 6px rgb(0 0 0 / 10%);
				}
				#tab_design .tab-options label img{
						width: 100%;
						height: auto;
						margin-bottom: 10px;
				}
				#tab_header.ui-tabs-panel label img,
				#tab_footer.ui-tabs-panel label img  {
				    width: 100%;
				    height: auto;
				    padding: 20px;
				}
				#tab_header.ui-tabs-panel label.active img {
				    box-shadow: 0 0 10px rgb(0 0 0 / 10%);
				}
				#tab_footer.ui-tabs-panel label.active img {
				    box-shadow: 0 0 10px rgb(0 0 0 / 10%);
				}
				#tab_elements .tab-options label {
						text-align: center;
					 text-transform: capitalize;
					 font-weight: 700;
					 margin-bottom: 10px;
					 font-size: 12px;
				}
				#tab_elements .tab-options label img {
				    width: auto;
				    height: auto;
				    border: 1px solid #d2d2d2;
				    margin-bottom: 5px;
				}
				@media(min-width:1024px){
					#lpc-options .tab-options label {
					    width: 32.33%;
					}
					#tab_elements .tab-options label {
					    width: 24%;
					}
				}
				@media(max-width:1023px) and (min-width:767px){
					#lpc-options .tab-options label {
					    width: 49%;
					}
					#tab_elements .tab-options label {
					    width: 49%;
					}
				}
				@media(max-width: 766px){
					#lpc-options .tab-options label {
					    width: 100%;
					}
					#tab_elements .tab-options label {
					    width: 100%;
					}
				}

				.tab-options input:checked + label img {
				    box-shadow: 0 0 8px rgb(0 0 0 / 10%);
				    border: 1px solid rgba(0, 0, 0, .05) !important;
				}
				#tab_elements .tab-options label:before {
				    display: none;
				}

				/*header options*/
				.lp-header-3 .top-bar .social-media-container {
				    display: none;
				}
				body:not(.lp-header-3) .top-header{
						display: none;
				}
				/*colour options*/
				.agent-profile .social-media a i{
					color: #fff !important;
				}
	</style>
	<?php
}
add_action('wp_head', 'lpc_insert_head');
