<?php
/*
* Plugin Name: Launchpad Customiser
* Plugin URI: https://launchpadwebsites.com.au/
* Description: Launchpad Customiser
* Version: 1.0.2
*/

require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/assets/style.php';
